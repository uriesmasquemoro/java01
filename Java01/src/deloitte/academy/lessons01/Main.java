package deloitte.academy.lessons01;

import deloitte.academy.lessons01.arithmetic.Arithmetic;
import deloitte.academy.lessons01.comparison.Comparison;
import deloitte.academy.lessons01.logical.Logical;

public class Main {

	public static void main(String[] args) {
		/*
		 * Arithmetic has static methods so we don't need to create an instance of the
		 * class
		 */
		Arithmetic.add(2, 4);
		Arithmetic.times(2, 4);
		Arithmetic.substract(10, 30);
		Arithmetic.divide(10, 0);
		Arithmetic.power(9, 3);

		/*
		 * Logical has static methods so we don't need to create an instance of the
		 * class
		 */
		Logical.lessThan(2, 4);
		Logical.moreThan(3, 4);
		Logical.lessEqualThan(4, 7);
		Logical.moreEqualThan(4, 8);
		Logical.equals(5, 5);

		/*
		 * Comparison has static methods so we don't need to create an instance of the
		 * class
		 */
		Comparison.isNegative(-8);
		Comparison.isPositive(6);
		Comparison.areEqual("Hola, mundo!", "Hola mundo!");
	}

}
