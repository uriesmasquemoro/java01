package deloitte.academy.lessons01.arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author rubemendoza
 * @version 0.1
 *
 *          This class has powerful, useful and, static methods that allows
 *          developer to perform complex arithmetic operations without
 *          instantiate the class itself
 */
public class Arithmetic {
	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());

	/**
	 * 
	 * This method performs a complex arithmetic calculus to let the user know the
	 * result two integer type numbers addition.
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return int the result of the arithmetic process to know the result of valueA
	 *         + valueB
	 */
	public static int add(int valueA, int valueB) {
		/*
		 * valueC storages the result of the 'add' method
		 */
		int valueC = 0;

		try {
			valueC = valueA + valueB;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve operation xd", e);
		}

		LOGGER.info("Returned value: " + valueC);
		return valueC;
	}

	/**
	 * 
	 * This method performs a complex arithmetic calculus to let the user know the
	 * result of two integer type numbers multiplication.
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return int the result of the arithmetic process to know the result of valueA
	 *         * valueB
	 */
	public static int times(int valueA, int valueB) {
		int valueC = 0;

		try {
			valueC = valueA * valueB;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve operation xd", e);
		}

		LOGGER.info("Returned value: " + valueC);
		return valueC;
	}

	/**
	 * 
	 * This method performs a complex arithmetic calculus to let the user know the
	 * result of two integer type numbers subtraction.
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return int the result of the arithmetic process to know the result of valueA
	 *         - valueB
	 */
	public static int substract(int valueA, int valueB) {
		int valueC = 0;

		try {
			valueC = valueA - valueB;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve operation xd", e);
		}

		LOGGER.info("Returned value: " + valueC);
		return valueC;
	}

	/**
	 * 
	 * This method performs a complex arithmetic calculus to let the user know the
	 * result of two integer type numbers division
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return int the result of the arithmetic process to know the result of valueA
	 *         / valueB
	 */
	public static float divide(int valueA, int valueB) {
		float valueC = 0.0f;

		try {
			valueC = (float) valueA / (float) valueB;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve operation xd", e);
		}

		LOGGER.info("Returned value: " + valueC);
		return valueC;
	}

	/**
	 * 
	 * This method performs a complex arithmetic calculus to let the user know the
	 * result of two integer type numbers power
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return int the result of the arithmetic process to know the result of valueA
	 *         ** valueB
	 */
	public static float power(int valueA, int valueB) {
		int valueC = valueA;

		try {
			for (int i = 0; i < valueB - 1; i++) {
				valueC *= valueA;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve operation xd", e);
		}

		LOGGER.info("Returned value: " + valueC);
		return valueC;
	}
}
