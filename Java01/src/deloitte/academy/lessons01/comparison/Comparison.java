package deloitte.academy.lessons01.comparison;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author rubemendoza
 * @version 0.1
 *
 *          This class has powerful, useful and, static methods that allows
 *          developer to perform complex comparison operations without
 *          instantiate the class itself
 */
public class Comparison {
	private static final Logger LOGGER = Logger.getLogger(Comparison.class.getName());

	/**
	 * 
	 * This method performs a complex validation to let the user know, given a
	 * number referred as valueA, if it belongs to the negative numbers set
	 * 
	 * @param valueA must be an integer type value
	 * @return Boolean if the condition is affirmative, otherwise if the if
	 *         statement does not proceed to return the if value, then a false value
	 *         will be returned.
	 */
	public static Boolean isNegative(int valueA) {
		if (valueA < 0) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}

	/**
	 * 
	 * This method performs a complex validation to let the user know, given a
	 * number referred as valueA, if it belongs to the positive numbers set
	 * 
	 * @param valueA must be an integer type value
	 * @return Boolean if the condition is affirmative, otherwise if the if
	 *         statement does not proceed to return the if value, then a false value
	 *         will be returned.
	 */
	public static Boolean isPositive(int valueA) {
		if (valueA >= 0) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}

	/**
	 * 
	 * This method performs a complex validation to let the user know, given two
	 * string (referred as firstString and secondString) if they are or not (if it
	 * was the case) equal.
	 * 
	 * @param firstString  must be an String object
	 * @param secondString must be an String object
	 * @return Boolean if the condition is affirmative, otherwise if the if
	 *         statement does not proceed to return the if value, then a false value
	 *         will be returned.
	 */
	public static Boolean areEqual(String firstString, String secondString) {
		try {
			if (firstString.equals(secondString)) {
				LOGGER.info("Returned value: " + true);
				return true;
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot resolve comparison xd", e);
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}
}
