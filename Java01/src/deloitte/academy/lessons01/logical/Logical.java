package deloitte.academy.lessons01.logical;

import java.util.logging.Logger;

/**
 * 
 * @author rubemendoza
 * @version 0.1
 *
 *          This class has powerful, useful and, static methods that allows
 *          developer to perform complex logical operations without instantiate
 *          the class itself
 */
public class Logical {
	private static final Logger LOGGER = Logger.getLogger(Logical.class.getName());

	/**
	 * 
	 * Allows user to know if valueA is less than valueB
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return Boolean if the comparison is true then the method will return a true
	 *         statement and, if it wasn't the it returns a false statement
	 */
	public static Boolean lessThan(int valueA, int valueB) {
		if (valueA < valueB) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}

	/**
	 * 
	 * Allows user to know if valueA is less-equal than valueB
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return Boolean if the comparison is true then the method will return a true
	 *         statement and, if it wasn't the it returns a false statement
	 */
	public static Boolean lessEqualThan(int valueA, int valueB) {
		if (valueA <= valueB) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}

	/**
	 * 
	 * Allows user to know if valueA is more than valueB
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return Boolean if the comparison is true then the method will return a true
	 *         statement and, if it wasn't the it returns a false statement
	 */
	public static Boolean moreThan(int valueA, int valueB) {
		if (valueA > valueB) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 * Allows user to know if valueA is more-equal than valueB
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return Boolean if the comparison is true then the method will return a true
	 *         statement and, if it wasn't the it returns a false statement
	 */
	public static Boolean moreEqualThan(int valueA, int valueB) {
		if (valueA >= valueB) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}

	/**
	 * 
	 * Allows user to know if valueA equals valueB
	 * 
	 * @param valueA must be an integer type value
	 * @param valueB must be an integer type value
	 * @return Boolean if the comparison is true then the method will return a true
	 *         statement and, if it wasn't the it returns a false statement
	 */
	public static Boolean equals(int valueA, int valueB) {
		if (valueA == valueB) {
			LOGGER.info("Returned value: " + true);
			return true;
		}

		LOGGER.info("Returned value: " + false);
		return false;
	}
}
